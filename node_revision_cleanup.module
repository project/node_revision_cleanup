<?php

/**
 * @file
 * Node revision cleanup module.
 *
 * This module provides a mechanism for mass deletion of node revisions for
 * databases of such a size that other modules may not suffice.
 */

/**
 * Implements hook_help().
 */
function node_revision_cleanup_help($path, $arg) {
  switch ($path) {
    case 'admin/help#node_revision_cleanup':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Node Revision Cleanup module is not intended for novice users or the Drupal layman. This module, currently, has no GUI interface and only have drush commands.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('This module is pretty straightforward, build a list of node revisions for a given content type, break that into manageable chunks (called jobs), work on each job sequentially until all of the revisions are cleaned up.') . '</p>';

      return $output;

    case 'admin/config/content/node_revision_delete':
      $output = '';
      $output .= '<p>' . t("To allow Node Revision Delete to act upon a certain content type, you should navigate to the desired content type's edit page via:") . '</p>';
      $output .= '<p><em>' . t('Administration » Structure » Content types » [Content type name]') . '</em></p>';
      $output .= '<p>' . t("Under the Publishing Options tab, enable 'Create new revision' and 'Limit the amount of revisions' checkboxes. Change the Maximum number of revisions to keep, if you need to, and finally, save your changes clicking in the Save content type button.") . '</p>';

      return $output;
  }
}

/**
 * Clean up after ourselves.
 *
 * @param string $content_type
 *   Clean up old data after finishing and before starting, just in case.
 */
function node_revision_cleanup_cruft($content_type) {
  $v = new NodeRevisionCleanupVariableCRUD();
  $jobs_available = $v->vget('node_revision_cleanup_' . $content_type . '_jobs_available', array());
  $count_jobs_available = count($jobs_available);
  if ($count_jobs_available > 0) {
    foreach ($jobs_available as $job) {
      $v->vdel('node_revision_cleanup_' . $content_type . '_job_number_' . $job);
    }
  }
  $v->vdel('node_revision_cleanup_' . $content_type . '_processes_running');
  $v->vdel('node_revision_cleanup_' . $content_type . '_total_vids');
  $v->vdel('node_revision_cleanup_' . $content_type . '_completed');
  $v->vdel('node_revision_cleanup_' . $content_type . '_jobs_available');
  $v->vdel('node_revision_cleanup_' . $content_type . '_lock');
  $v->vdel('node_revision_cleanup_' . $content_type . '_begin');
  $v->vdel('node_revision_cleanup_' . $content_type . '_completed');
}

/**
 * Convert seconds into something more meaningful.
 *
 * @param int $secs
 *   A UNIX timestamp.
 *
 * @return array
 *   An array broken into hours, minutes and seconds.
 */
function _node_revision_cleanup_seconds_to_time($secs) {
  $dt = new DateTime('@' . $secs, new DateTimeZone('UTC'));
  return array(
    'days' => (int) $dt->format('z'),
    'hours' => (int) $dt->format('G'),
    'minutes' => (int) $dt->format('i'),
    'seconds' => (int) $dt->format('s'),
  );
}
